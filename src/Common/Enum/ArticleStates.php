<?php


namespace App\Common\Enum;


class ArticleStates
{
    public const displayed = "Affiché";
    public const undisplayed = "Masqué";
}