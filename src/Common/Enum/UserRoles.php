<?php


namespace App\Common\Enum;


class UserRoles
{
    public const admin = "ROLE_ADMIN";
    public const user = "ROLE_USER";
}