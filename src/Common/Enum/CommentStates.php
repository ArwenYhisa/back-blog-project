<?php


namespace App\Common\Enum;


class CommentStates
{
    public const approved = "Approved";
    public const waiting = "Waiting";
    public const reject = "Reject";
}