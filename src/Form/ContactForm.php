<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //TODO: bloquer le numéro à 10 chiffres, je sais pas encore comment...
        $builder
            ->add('firstname', TextType::class, ["label"=>"Prénom"])
            ->add('lastname', TextType::class, ["label"=>"Nom"])
            ->add('email', EmailType::class, ["label"=>"Adresse email"])
            ->add('phone', NumberType::class, ["label"=>"Numéro de téléphone", 'attr'=>['min'=>'10', 'max'=>'10']])
            ->add('message', TextareaType::class, ["label"=>"Votre message"])
        ;
    }
}