<?php


namespace App\Form;


use App\Common\Enum\CommentStates;
use ReflectionClass;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchByStateForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $states = new ReflectionClass(CommentStates::class);
        $states = $states->getConstants();
        $statesName = [];
        foreach ($states as $key => $value) {
            $statesName[$value] = $value;
        }

        $builder
            ->add('state', ChoiceType::class, ["choices" => $statesName, "label" => "Trier les commentaires par état"])
            ->add('search', SubmitType::class)
        ;
    }
}