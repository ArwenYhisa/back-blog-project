<?php


namespace App\Form;


use App\Common\Enum\ArticleStates;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ArticleForm extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $status = new ReflectionClass(ArticleStates::class);
        $status = $status->getConstants();
        $statusName = [];
        foreach ($status as $key => $value) {
            $statusName[$value] = $value;
        }

        $category = $this->entityManager->getRepository(Category::class)->findAll();
        $categoryName = [];
        foreach ($category as $cat) {
            $categoryName[$cat->getName()] = $cat;
        }

        $builder
            ->add('title', TextType::class, ["label"=>"Titre"])
            ->add('content', TextareaType::class, ["label"=>"Corps de l'article"])
            ->add('status', ChoiceType::class, ["choices" => $statusName, "label"=>"Statut"])
            ->add('category', ChoiceType::class, ["choices" => $categoryName, "label"=>"Catégorie"])
            ->add('image', FileType::class, ["label"=>"Image de l'article", "mapped" => false])
        ;
    }
}