<?php


namespace App\Form;


use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SearchByCategoryForm extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $category = $this->entityManager->getRepository(Category::class)->findAll();
        $categoryName = [];
        foreach ($category as $cat) {
            $categoryName[$cat->getName()] = $cat;
        }

        $builder
            ->add('category', ChoiceType::class, ["choices" => $categoryName, "label" => "Trier par catégorie"])
            ->add('search', SubmitType::class)
        ;
    }
}