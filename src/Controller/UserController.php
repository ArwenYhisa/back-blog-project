<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use App\Form\UserInformationForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */

     public function index(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $existingUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(["email"=>$user->getEmail()]);
            if ($existingUser){
                return $this->redirectToRoute("User already exists");
            }
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $entity = $this->getDoctrine()->getManager();
            $entity->persist($user);
            $entity->flush();

            return $this->redirectToRoute("home");
        }
        return $this->render('user/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profile(Request $request):Response
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(["email" => $this->getUser()->getUsername()]);
        $form = $this->createForm(UserInformationForm::class, $user, array(
            'method' => 'PUT'
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return $this->render('user/profile.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
