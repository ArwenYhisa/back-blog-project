<?php


namespace App\Controller;


use App\Entity\Message;
use App\Form\ContactForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/about", name="about")
     */
    public function aboutBlog()
    {
        return $this->render('default/aboutBlog.html.twig',[
            'imagePath' => $_ENV['APP_URL'] . '/' . $this->getParameter('article_image_directory')
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request):Response
    {
        $form = $this->createForm(ContactForm::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $message = new Message();
            $message->setFirstname($form->get('firstname')->getData());
            $message->setLastname($form->get('lastname')->getData());
            $message->setEmail($form->get('email')->getData());
            $message->setPhone($form->get('phone')->getData());
            $message->setMessage($form->get('message')->getData());

            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();
        }

        return $this->render('default/contact.html.twig',[
            'form' => $form->createView(),
            'imagePath' => $_ENV['APP_URL'] . '/' . $this->getParameter('article_image_directory')
        ]);
    }
}