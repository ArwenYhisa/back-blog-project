<?php

namespace App\Controller;


use App\Common\Enum\ArticleStates;
use App\Common\Enum\CommentStates;
use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Like;
use App\Entity\User;
use App\Form\ArticleForm;
use App\Form\CommentForm;
use App\Form\SearchByCategoryForm;
use App\Form\SearchByTitleForm;
use Doctrine\Common\Util\Debug;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article/create", name="create_article")
     * @param Request $request
     * @return Response
     */
    public function createArticle(Request $request): Response
    {
        $form = $this->createForm(ArticleForm::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//            Debug::dump($form->get('title')->getData());
            $article = new Article();
            $article->setTitle($form->get('title')->getData());
            $article->setContent($form->get('content')->getData());
            if ($form->get('status')->getData() == ArticleStates::displayed) {
                $article->setStatus(1);
            } else {
                $article->setStatus(0);
            }
            $article->setCategory($form->get('category')->getData());

            $currentUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(["email" => $this->getUser()->getUsername()]);
            $article->setAuthor($currentUser);
            $article->setDate(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            /**
             * @var $image UploadedFile
             */
            $image = $form->get('image')->getData();
            $imageName = $article->getId() . "." . $image->getClientOriginalExtension();
            try {
                $image->move(
                    $this->getParameter('article_image_directory'),
                    $imageName
                );
            } catch (FileException $e) {
                print_r($e->getMessage());
            }
            $article->setImage($imageName);
            $em->flush();
            return $this->redirectToRoute('list_articles');
        }
        return $this->render('article/createArticle.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/article/{id}", requirements={"id"="\d+"}, name="show_article")
     * @param int $id
     * @return Response
     */
    public function showArticle(int $id, Request $request): Response
    {
        //TODO: Si l'article n'existe pas renvoyer sur "Article 404 ect"
        $form = $this->createForm(CommentForm::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment = new Comment();
            $comment->setContent($form->get('content')->getData());
            $comment->setDate(new \DateTime());
            $comment->setState(CommentStates::waiting);
            $currentUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(["email" => $this->getUser()->getUsername()]);
            $comment->setUser($currentUser);
            $comment->setArticle($this->getDoctrine()->getRepository(Article::class)->find($id));

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

        }

        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        $comments = $article->getComments();

        if ($request->request->count() > 0) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
            return $this->redirectToRoute('list_articles');
        }

        return $this->render("article/showArticle.html.twig", [
            'form' => $form->createView(),
            'article' => $article,
            'comments' => $comments,
            'imagePath' => $_ENV['APP_URL'] . '/' . $this->getParameter('article_image_directory'),
            'commentStatesDisplayed' => CommentStates::approved
        ]);
    }

    /**
     * @Route("/", name="list_articles")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function listArticles(Request $request, PaginatorInterface $paginator): Response
    {
        $categoryForm = $this->createForm(SearchByCategoryForm::class);
        $categoryForm->handleRequest($request);
        $titleForm = $this->createForm(SearchByTitleForm::class);
        $titleForm->handleRequest($request);

        if ($categoryForm->isSubmitted() && $categoryForm->isValid()) {
            $articles = $this->getDoctrine()
                ->getRepository(Article::class)
                ->findBy(['category' => $categoryForm->get('category')->getData()], array('date' => 'desc'));
        } elseif ($titleForm->isSubmitted() && $titleForm->isValid()) {
            $articles = $this->getDoctrine()
                ->getRepository(Article::class)
                ->findBy(['title' => $titleForm->get('title')->getData()], array('date' => 'desc'));
        } else {
            $articles = $this->getDoctrine()
                ->getRepository(Article::class)
                ->findBy(['status'=>ArticleStates::displayed], array('date' => 'desc'));

            if ($request->request->count() > 0) {
                $like = new Like();

                $currentUser = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findOneBy(["email" => $this->getUser()->getUsername()]);
                $like->setUser($currentUser);

                $article = $this->getDoctrine()
                    ->getRepository(Article::class)
                    ->findOneBy(["id" => $request->request->get("like")]);
                $like->setArticle($article);

                $em = $this->getDoctrine()->getManager();
                $em->persist($like);
                $em->flush();

            }
        }

        $articlesPaginate = $paginator->paginate(
            $articles,
            $request->query->getInt('page', 1),
            5
        );
        return $this->render("article/listArticles.html.twig", [
            'categoryForm' => $categoryForm->createView(),
            'titleForm' => $titleForm->createView(),
            'articles' => $articlesPaginate,
            'imagePath' => $_ENV['APP_URL'] . '/' . $this->getParameter('article_image_directory')
        ]);
    }

    /**
     * @Route("/article/edit/{id}", requirements={"id"="\d+"}, name="edit_article")
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function editArticle(int $id, Request $request): Response
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        $form = $this->createForm(ArticleForm::class, $article, array(
            'method' => 'PUT'
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setTitle($form->get('title')->getData());
            $article->setContent($form->get('content')->getData());
            if ($form->get('status')->getData() == ArticleStates::displayed) {
                $article->setStatus(1);
            } else {
                $article->setStatus(0);
            }
            $article->setCategory($form->get('category')->getData());

            $currentUser = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneBy(["email" => $this->getUser()->getUsername()]);
            $article->setAuthor($currentUser);
            $article->setDate(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            /**
             * @var $image UploadedFile
             */
            $image = $form->get('image')->getData();
            $imageName = $article->getId() . "." . $image->getClientOriginalExtension();
            try {
                $image->move(
                    $this->getParameter('article_image_directory'),
                    $imageName
                );
            } catch (FileException $e) {
                print_r($e->getMessage());
            }
            $article->setImage($imageName);
            $em->flush();

        }

        return $this->render('article/editArticle.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route ("/like-share", name="like_and_share")
     * @return Response
     */
    public function likeAndShare(): Response
    {
        $currentUser = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(["email" => $this->getUser()->getUsername()]);

        $likes = $this->getDoctrine()
            ->getRepository(Like::class)
            ->findBy(['user' => $currentUser->getId()]);

        $articles = [];
        foreach ($likes as $like) {
            $articles[] = $like->getArticle();
        }
        if (count($articles) > 5){
            $articles = array_slice($articles, 0, 5);
        }

        return $this->render('user/likeAndShare.html.twig', [
            'likeArticle' => $articles,
            'imagePath' => $_ENV['APP_URL'] . '/' . $this->getParameter('article_image_directory')
        ]);
    }

    /**
     * @Route ("/article/undisplayed", name="undisplayed_articles")
     */
    public function articlesStates(Request $request): Response
    {
        $states = new ReflectionClass(ArticleStates::class);
        $states = $states->getConstants();
        $statesName = [];
        foreach ($states as $key => $value) {
            $statesName[$value] = $value;
        }

        if ($request->request->count() > 0){
            $article = $this->getDoctrine()
                ->getRepository(Article::class)
                ->findOneBy(['id'=>$request->request->get('articleId')]);

            if ($request->request->get('state') == ArticleStates::displayed) {
                $article->setStatus(1);
            } else {
                $article->setStatus(0);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
        }

        $articles = $this->getDoctrine()
            ->getRepository(Article::class)
            ->findBy(['status'=>0]);

        return $this->render('article/undisplayed_articles.html.twig', [
            'articles'=>$articles,
            'states'=>$statesName
        ]);
    }
}