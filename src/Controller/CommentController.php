<?php


namespace App\Controller;


use App\Common\Enum\CommentStates;
use App\Entity\Comment;
use App\Form\ReviewCommentForm;
use App\Form\SearchByStateForm;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends AbstractController
{
    /**
     * @Route("/comments", name="list_comments")
     * @param Request $request
     * @return Response
     */
    public function showComments(Request $request): Response
    {
        $states = new ReflectionClass(CommentStates::class);
        $states = $states->getConstants();
        $statesName = [];
        foreach ($states as $key => $value) {
            $statesName[$value] = $value;
        }

        //Trier les commentaires par états
        $searchByState = $this->createForm(SearchByStateForm::class);
        $searchByState->handleRequest($request);
        if ($searchByState->isSubmitted() && $searchByState->isValid()){
            $comments = $this->getDoctrine()
                ->getRepository(Comment::class)
                ->findBy(['state'=>$searchByState->get('state')->getData()], array('date' => 'desc'));
        }
        else {
            $comments = $this->getDoctrine()
                ->getRepository(Comment::class)
                ->findBy(['state'=>CommentStates::waiting], array('date' => 'desc'));
        }

        // Update d'un commentaire via la méthode POST
        if ($request->request->count() > 0 && !$searchByState->isSubmitted()) {
            $comment = $this->getDoctrine()
                ->getRepository(Comment::class)
                ->findOneBy(['id'=>$request->request->get('commentId')]);
            $comment->setState($request->request->get('state'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
        }

        return $this->render("comment/listComments.html.twig", [
            'form'=> $searchByState->createView(),
           'comments' => $comments,
            'commentStates' => $statesName
        ]);
    }
}