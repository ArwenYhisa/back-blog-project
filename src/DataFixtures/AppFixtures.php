<?php

namespace App\DataFixtures;

use App\Common\Enum\CommentStates;
use App\Common\Enum\UserRoles;
use App\Common\LoremIpsum;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use ReflectionClass;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $faker->seed(0);
        $user1 = new User();
        $user1
            ->setEmail('user@ex1.com')
            ->setPassword($this->encoder->encodePassword($user1, 'user'))
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setPseudo($faker->firstName)
        ;
        $manager->persist($user1);
        $manager->flush();

        $faker = Factory::create('fr_FR');
        $faker->seed(0);
        $user2 = new User();
        $user2
            ->setEmail('user@ex2.com')
            ->setPassword($this->encoder->encodePassword($user2, 'user'))
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setPseudo($faker->firstName)
        ;
        $manager->persist($user2);
        $manager->flush();

        $admin = new User();
        $admin
            ->setEmail('admin@ex.com')
            ->setPassword($this->encoder->encodePassword($admin, 'admin'))
            ->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setPseudo($faker->firstName)
            ->setRoles([UserRoles::admin])
        ;
        $manager->persist($admin);
        $manager->flush();

        $categorie1 = new Category("Voyage");
        $categorie2 = new Category("Nourriture");
        $categorie3 = new Category("Travail");
        $manager->persist($categorie1);
        $manager->persist($categorie2);
        $manager->persist($categorie3);
        $manager->flush();

        $categories = [$categorie1, $categorie2, $categorie3];
        $images = ['voyage.jpg', 'nourriture.jpg', 'travail.jpg'];
        $states = new ReflectionClass(CommentStates::class);
        $states = $states->getConstants();
        $commentStates = [];
        foreach ($states as $key => $value) {
            $commentStates[] = $value;
        }
        $users = [$user1, $user2];


        for ($i = 1; $i <= 50; $i++){
            $cat = random_int(0, 2);
            $article = new Article();
            $article
                ->setTitle("Lorem Ipsum")
                ->setContent($faker->sentence(random_int(15, 40)))
                ->setStatus(random_int(0, 1))
                ->setCategory($categories[$cat])
                ->setAuthor($admin)
                ->setDate($faker->dateTime)
                ->setImage($images[$cat])
            ;
            $manager->persist($article);
            $nbComments = random_int(1, 3);
            for ($j = 0; $j <= $nbComments; $j++){
                $comment = new Comment();
                $comment
                    ->setContent($faker->sentence(random_int(3, 20)))
                    ->setDate($faker->dateTime)
                    ->setState($commentStates[random_int(0, 2)])
                    ->setUser($users[random_int(0, 1)])
                    ->setArticle($article)
                ;
                $article->addComment($comment);
                $manager->persist($comment);
            }
            $manager->flush();
        }
    }
}
